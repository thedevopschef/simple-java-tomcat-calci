# What we will do?

### We will develop a basic maven web application in Java using DevOps tools like Apache Maven as a build tool, GitLab for source code management, Selenium for testing, Jenkins for Continuous integration and Apache Tomcat for deployement and use jenkins plugin to monitor our pipeline.

### All things will be done under one Jenkins Pipeline!

## For example we will develop a basic four function Calculator!

### STEP-1  : Write the sourcecode for a basic four function Calculator

[To see all the source code click!](/src/main)

### STEP-3  : Install all the dependencies!

Install Java, Maven, Tomcat on the build agent


### STEP-4  : Click on Build now and start building your project

what it will do?
+ Fetch your sourcecode from github
+ Compile it
+ Make its build ( a .war file)
+ Test it using selenium (basic)
+ and then deploy it to a tomcat server

pipeline will do all its job stage by stage and you will get outputs for every successfull job!


### STEP-5  : Start monitoring!

first install a delivery pipeline view plugin in your jenkins 
it will be showing two views
+ one for a dashboard
+ other a pipeline view

you can monitor all it changes, time, start time, end time ,logs etc


### STEP-6  : Check your Web Application

open your tomcat url along with the context path provided

##      THANK YOU  :blush: 





